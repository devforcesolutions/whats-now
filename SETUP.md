- Prerequisiti:

    1) E' preferibile (di gran lunga) utilizzare intelliJ anzichè Eclipse (la versione Ultimate è 
    disponibile per gli studenti utilizzando l'indirizzo email universitario).
    2) Maven 3.5.4 o superiore installato e configurato su intelliJ.
    3) PostgreSQL server 9.6.6 o superiore installato localmente (porta 5432, username: postgres, 
    password: postgres).
    4) Insomnia REST client installato (per effettuare chiamate REST senza client);
    

- Setup:
    0) Creare lo schema SQL:
        1) aprire un cmd e digitare psql -U postgres (se necessario inserire la password postgres)
        2) eseguire il comando **CREATE DATABASE whatsnow_db;**
        3) chiudere la console appena l'operazione sarà completata.
    1) Aprire il progetto con intelliJ (Open Project -> selezionare il pom.xml padre -> Open as 
    Project)
    2) Una volta che intelliJ avrà concluso l'indexing del progetto selezionare in basso a destra il ramo 
    attuale (WN-dev-1.0-fix). Successivamente andare su Run -> Edit Configurations.
    3) Cliccare sul + in alto a sinistra e selezionare "Maven" come categoria
    4) Configurare come segue:
        1) Name: "DB-Reset"
        2) Working directory: <selezionare la root del modulo /whats-now/wn-server/wn-server-database
         dal pulsante sulla destra>
         3) Command line: 'flyway:clean flyway:migrate' senza apici.
    5) Configurare il datasource del database su intelliJ (non indispensabile ma decisamente utile, 
    solo per le verisoni Ultimate) altrimenti scaricare DataGrip o PgAdmin.
    6) Dal terminale di intelliJ (CTRL + F12) lanciare il comando '**mvn clean install**'
    7) IntelliJ dovrebbe riconoscere il progetto come progetto Spring-Boot, aggiungendo una tab 
    'Run Dashboard' in basso a destra.
    
- Avvio:

    1) Andare nella dashboard e lanciare il modulo 'WhatsNowServer' (ovvero la versione 
    all-in-one del progetto. E' anche possibile avviare ogni modulo singolarmente su porte 
    differenti).
    2) Se il deploy è andato a buon fine è possibile aprire una scheda del browser e visitare 
    localhost:8090, che dovrebbe mostrare una pagina di test.
    3) Aprire Insomnia ed andare in Application -> Preferences e scegliere la scheda 'Data'.
    4) Cliccare su Import Data -> From file e selezionare il file Insomnia_?data?.har presente 
    nella root del progetto.
    5) Provare ad inviare il comando di LoginWithPassword (dovrebbe essere precompilato), se la 
    risposta ci dà stato 200, 
    l'applicazione è 
    stata correttamrente configurata.

